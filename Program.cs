﻿using System;
using System.Linq;
using System.Reflection;
using Challenges.src;

namespace Challenges
{
    class Program
    {
        static void Main(string[] args)
        {
            var challenges = Assembly.GetExecutingAssembly()
                .GetTypes()
                .Where(t => t.GetInterface(nameof(IChallenge)) is { });

            foreach (var type in challenges)
            {
                var challenge = Activator.CreateInstance(type) as IChallenge;
                challenge!.Run();
                Console.WriteLine($"{type.Name} OK");
            }
        }
    }
}
