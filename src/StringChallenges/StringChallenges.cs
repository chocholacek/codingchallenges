using System;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Challenges.src;
using Shouldly;

namespace Challenges.src.StringChallenges
{
    public class StringChallenges : IChallenge
    {

        /// <summary>
        /// A pair of strings form a strange pair if both of the following are true:
        ///     The 1st string's first letter = 2nd string's last letter.
        ///     The 1st string's last letter = 2nd string's first letter.
        /// Create a function that returns true if a pair of strings constitutes a strange pair, and false otherwise.
        /// </summary>
        /// <param name="first"></param>
        /// <param name="second"></param>
        /// <returns></returns>
        public bool IsStrangePair(string first, string second) => (first, second) switch
        {
            ("", "") => true,
            ("", _) => false,
            (_, "") => false,
            var (a, b) => a[0] == b[^1] && a[^1] == b[0]
        };

        /// <summary>
        /// Usually when you sign up for an account to buy something, your credit card number, phone number or answer to a 
        /// secret question is partially obscured in some way. Since someone could look over your shoulder, you don't want 
        /// that shown on your screen. Hence, the website masks these strings.
        ///
        /// Your task is to create a function that takes a string, transforms all but the last four characters 
        /// into "#" and returns the new masked string
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public string Maskify(string str) => str switch
        {
            var s when s.Length > 4 => str[^4..].PadLeft(str.Length, '#'),
            _ => str
        };

        /// <summary>
        /// Create a function that accepts a string of space separated numbers and returns the highest and lowest number (as a string).
        /// 
        /// Notes:
        ///     All numbers are valid Int32, no need to validate them.
        ///     There will always be at least one number in the input string.
        ///     Output string must be two numbers separated by a single space, and highest number is first.
        /// </summary>
        /// <param name="numbers"></param>
        /// <returns></returns>
        public string HighLow(string numbers)
        {
            var parsed = numbers.Split(' ').Select(s => int.Parse(s));
            var (min, max) = (parsed.Min(), parsed.Max());
            return $"{max} {min}";
        }

        /// <summary>
        /// Create a function that determines whether a string is a valid hex code.
        /// 
        /// A hex code must begin with a pound key # and is exactly 6 characters in length. 
        /// Each character must be a digit from 0-9 or an alphabetic character from A-F. 
        /// All alphabetic characters may be uppercase or lowercas
        /// </summary>
        /// <param name="hex"></param>
        /// <returns></returns>
        public bool IsValidHexCode(string hex) => Regex.IsMatch(hex, @"^#[\da-fA-F]{6}$");

        /// <summary>
        /// Carlos is a huge fan of something he calls smooth sentences. A smooth sentence is one 
        /// where the last letter of each word is identical to the first letter the following word.
        /// 
        /// To illustrate, the following would be a smooth sentence: "Carlos swam masterfully."
        /// 
        /// Since "Carlos" ends with an "s" and swam begins with an "s" and swam ends with an "m" and masterfully begins with an "m".
        /// </summary>
        /// <param name="sentence"></param>
        /// <returns></returns>
        public bool IsSmooth(string sentence)
        {
            var words = sentence.Split(' ').ToArray();

            for (var i = 0; i < words.Length - 1; ++i)
            {
                if (words[i][^1] != words[i + 1][0])
                    return false;
            }

            return true;
        }

        /// <summary>
        /// Create a function that takes a strings characters as ASCII and returns each characters hexadecimal value as a string.
        /// 
        /// Notes:
        ///     Each byte must be seperated by a space.
        ///     All alpha hex characters must be lowercase.
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public string ConvertToHex(string message) => message.ToArray()
            .Select(i => $"{(int)i:x2}")
            .Aggregate(new StringBuilder(), (sb, s) => 
            { 
                sb.Append(s); 
                return sb.Append(" "); 
            })
            .ToString()
            .TrimEnd(' ');

        public void Run()
        {
            // IsStrangePair
            IsStrangePair("ratio", "orator").ShouldBeTrue();
            IsStrangePair("sparkling", "groups").ShouldBeTrue();
            IsStrangePair("bush", "hubris").ShouldBeFalse();
            IsStrangePair("", "").ShouldBeTrue();

            Maskify("4556364607935616").ShouldBe("############5616");
            Maskify("64607935616").ShouldBe("#######5616");
            Maskify("1").ShouldBe("1");
            Maskify("").ShouldBe("");

            HighLow("1 2 3 4 5").ShouldBe("5 1");
            HighLow("1 2 -3 4 5").ShouldBe("5 -3");
            HighLow("1 9 3 4 -5").ShouldBe("9 -5");
            HighLow("13").ShouldBe("13 13");

            IsValidHexCode("#CD5C5C").ShouldBeTrue();
            IsValidHexCode("#EAECEE").ShouldBeTrue();
            IsValidHexCode("#eaecee").ShouldBeTrue();
            IsValidHexCode("#CD5C58C").ShouldBeFalse();
            IsValidHexCode("#CD5C5Z").ShouldBeFalse();
            IsValidHexCode("#CD5C&C").ShouldBeFalse();
            IsValidHexCode("CD5C5C").ShouldBeFalse();

            IsSmooth("Marta appreciated deep perpendicular right trapezoids").ShouldBeTrue();
            IsSmooth("Someone is outside the doorway").ShouldBeFalse();
            IsSmooth("She eats super righteously").ShouldBeTrue();

            ConvertToHex("hello world").ShouldBe("68 65 6c 6c 6f 20 77 6f 72 6c 64");
            ConvertToHex("Big Boi").ShouldBe("42 69 67 20 42 6f 69");
            ConvertToHex("Marty Poppinson").ShouldBe("4d 61 72 74 79 20 50 6f 70 70 69 6e 73 6f 6e");
        }
    }
}