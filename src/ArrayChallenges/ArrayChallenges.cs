using System.Linq;
using MarziBits;
using Shouldly;

namespace Challenges.src.ArrayChallenges
{
    public class ArrayChallenges : IChallenge
    {
        /// <summary>
        /// Create a function that takes an array of positive and negative numbers. Return an array where the first element 
        /// is the count of positive numbers and the second element is the sum of negative numbers.
        /// </summary>
        /// <param name="numbers"></param>
        /// <returns></returns>
        public int[] CountPosSumNeg(int[] numbers)
        {
            var grouping = numbers.GroupBy(n => n > 0).ToDictionary(g => g.Key, g => g);

            if (grouping.Count == 0)
                return new int[0];

            return new []
            {
                grouping[true].Count(),
                grouping[false].Sum()
            };
        }

        /// <summary>
        /// Create a function that takes two numbers as arguments (num, length) and returns an array of multiples of num up to length.
        /// </summary>
        /// <param name="num"></param>
        /// <param name="lenght"></param>
        /// <returns></returns>
        public int[] ArrayOfMultiples(int num, int lenght) => Sequences.ArithmeticSequence(num, num)
            .Take(lenght)
            .ToArray();

        public void Run()
        {
            CountPosSumNeg(new [] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, -11, -12, -13, -14, -15 })
                .ShouldBe(new [] { 10, -65 });
            CountPosSumNeg(new [] { 92, 6, 73, -77, 81, -90, 99, 8, -85, 34 })
                .ShouldBe(new [] { 7, -252});
            CountPosSumNeg(new [] { 91, -4, 80, -73, -28 }).ShouldBe(new [] { 2, -105 });
            CountPosSumNeg(new int[0]).ShouldBe(new int[0]);

            ArrayOfMultiples(7, 5).ShouldBe(new [] { 7, 14, 21, 28, 35 });
            ArrayOfMultiples(12, 10).ShouldBe(new [] { 12, 24, 36, 48, 60, 72, 84, 96, 108, 120 });
            ArrayOfMultiples(17, 6).ShouldBe(new [] { 17, 34, 51, 68, 85, 102 });
        }
    }
}