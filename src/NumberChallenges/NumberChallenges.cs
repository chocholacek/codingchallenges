using System;
using System.Linq;
using System.Text.RegularExpressions;
using MarziBits;
using Shouldly;

namespace Challenges.src.NumberChallenges
{
    public class NumberChallenges : IChallenge
    {
        /// <summary>
        /// Create a function that takes any nonnegative number as an argument and return it with it's digits in descending order. 
        /// Descending order is when you sort from highest to lowest
        /// </summary>
        /// <returns></returns>
        public uint SortDecending(uint number)
        {
            var chars = number.ToString()
                .ToCharArray()
                .OrderByDescending(c => c)
                .ToArray();

            return uint.Parse(chars);
        }

        /// <summary>
        /// Create a function that takes in n, a, b and returns the number of values raised to the nth power that 
        /// lie in the range [a, b], inclusive.
        /// 
        /// Notes:
        ///     Remember that the range is inclusive.
        ///     a < b will always be true.
        /// </summary>
        /// <param name="n"></param>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public int PowerRanger(int n, int a, int b) => Sequences.ArithmeticSequence()
            .Select(x => Math.Pow(x, n))
            .SkipWhile(x => x < a)
            .TakeWhile(x => x >= a && x <= b)
            .Count();

        /// <summary>
        /// Create a function that takes two integers and returns true if a number repeats three times 
        /// in a row at any place in num1 AND that same number repeats two times in a row in num2.
        /// 
        /// Notes:
        ///     You can expect every test case to contain exactly two integers.
        /// </summary>
        /// <param name="num1"></param>
        /// <param name="num2"></param>
        /// <returns></returns>
        public bool Trouble(long num1, long num2)
        {
            var str1 = num1.ToString();
            var str2 = num2.ToString();

            for (int i = 0; i < 10; ++i)
            {
                if (Regex.IsMatch(str1, $"^.*{i}{{3}}.*$") && Regex.IsMatch(str2, $"^.*{i}{{2}}.*$"))
                    return true;
            }
            return false;
        }

        public void Run()
        {
            SortDecending(123u).ShouldBe(321u);
            SortDecending(2619805u).ShouldBe(9865210u);
            SortDecending(73065u).ShouldBe(76530u);

            PowerRanger(2, 49, 65).ShouldBe(2);
            PowerRanger(3, 1, 27).ShouldBe(3);
            PowerRanger(10, 1, 5).ShouldBe(1);
            PowerRanger(5, 31, 33).ShouldBe(1);
            PowerRanger(4, 250, 1300).ShouldBe(3);

            Trouble(451999277, 41177722899).ShouldBeTrue();
            Trouble(1222345, 12345).ShouldBeFalse();
            Trouble(666789, 12345667).ShouldBeTrue();
            Trouble(33789, 12345337).ShouldBeFalse();
        }
    }
}