using System;
using System.Collections;
using System.Linq;
using Shouldly;

namespace Challenges.src.BinaryChallenges
{
    public class BinaryChallenges : IChallenge
    {
        /// <summary>
        /// Count the amount of ones in the binary representation of an integer. 
        /// So for example, since 12 is '1100' in binary, the return value should be 2
        /// </summary>
        /// <param name="argument"></param>
        /// <returns></returns>
        public int CountOnes(int argument)
        {
            var bytes = BitConverter.GetBytes(argument);
            var bits = new BitArray(bytes);
            return bits.OfType<bool>().Count(bit => bit);
        }

        public void Run()
        {
            CountOnes(0).ShouldBe(0);
            CountOnes(100).ShouldBe(3);
            CountOnes(999).ShouldBe(8);
        }
    }
}